<?php
class membershipModelCfs extends modelCfs {
	protected $tableName;
	protected $memberShipClassName;

	public function __construct() {
		global $wpdb;
		$this->tableName = $wpdb->prefix . 'cfs_membership_presets';
		$this->memberShipClassName = 'SupsysticMembership';
		//$this->_setTbl('membership_presets');
	}

	public function isPluginActive() {
		$tableExistsQuery =  "SHOW TABLES LIKE '" . $this->tableName . "'";
		$results = dbCfs::get($tableExistsQuery);
		
		if(count($results) && class_exists($this->memberShipClassName)) {
			return true;
		}
		return false;
	}

	public function getPluginInstallUrl() {
		return add_query_arg(
			array(
				's' => 'Membership by Supsystic',
				'tab' => 'search',
				'type' => 'term',
			),
			admin_url( 'plugin-install.php' )
		);
	}

	public function updateRow($params) {
		if(isset($params['form_id']) && isset($params['allow_use'])) {
			$allowUse = (int)$params['allow_use'];
			$formId = (int)$params['form_id'];

			if($formId && isset($allowUse)) {
				$query = "INSERT INTO `" . $this->tableName . "`(`form_id`, `allow_use`)"
					. " VALUES (" . $formId . ", " . $allowUse . ") "
					. "ON DUPLICATE KEY UPDATE `allow_use`=" . $allowUse;

				$res = dbCfs::query($query);
				return $res;
			}
		}
		return false;
	}

	public function removeRowByFormId($formId) {
		$query = "DELETE FROM " . $this->tableName
			. " WHERE `form_id`=" . (int) $formId;

		$res = dbCfs::query($query);
		return $res;
	}
}
