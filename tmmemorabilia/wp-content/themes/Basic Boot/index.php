<?php get_header(); ?>
<div class="container">
      <div class="row">
	      <div class="col-md-12">
          <!--<h1 class="text-center">Helping devoted sports fans reconnect with the past so they can relive treasured sports memories.</h1>-->
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                  </ol>
                
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                      <img src="http://cdn.jssor.com/demos/img/home/02.jpg" alt="Chania">
                    </div>
                
                    <div class="item">
                      <img src="http://cdn.jssor.com/demos/img/home/02.jpg" alt="Chania">
                    </div>
                
                    <div class="item">
                      <img src="http://cdn.jssor.com/demos/img/home/02.jpg" alt="Flower">
                    </div>
                
                    <div class="item">
                      <img src="http://cdn.jssor.com/demos/img/home/02.jpg" alt="Flower">
                    </div>
                  </div>
                
                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <img class="margintop120" src="http://tmmemorabilia.kylakiefer.com/wp-content/uploads/2016/11/fa-chevron-left_100_0_509623_none.png" width="50" height="50" valign="middle">
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <img class="margintop120" src="http://tmmemorabilia.kylakiefer.com/wp-content/uploads/2016/11/fa-chevron-right.png" width="50" height="50" valign="middle">
                    <span class="sr-only">Next</span>
                  </a>
                </div>
	      </div>
      </div>
</div>
<div class="fundraising">
	<div class="container">
      <div class="row">
	      	<div class="col-md-12 text-center">
				<div class="strike">
    				<span class="headerh1">Fundraising</span>
				</div>
		  	</div>
		  	<div class="col-md-4 col-xs-12">
				<h2 class="text-center">Helping your cause</h2>
				<p>Our unique approach allows you to display another class of attractive items for your silent/live auctions and raffles to create a new revenue funding channel</p>
			</div>
			<div class="col-md-4 col-xs-12">
				<h2 class="text-center">Zero risk to you</h2>
				<p>No financial obligation or risk. <br>
				No upfront costs or hidden fees. <br>
				All pricing provided before the event.<br>
				No payment until conclusion.<br>
				All items on consignment basis.</p>
			</div>
			<div class="col-md-4 col-xs-12">
				<h2 class="text-center">Authenticity</h2>
				<p>We provide 100% authentic sports memorabilia.<br>
				All autographed items come with a tamper-proof serial number sticker or hologram and a certificate of authenticty.</p>
			</div>
			<div class="col-md-12">
				<h2 class="text-right learn"><a href="http://tmmemorabilia.kylakiefer.com/index.php/fundraising/">Learn More</a></h2>
			</div>    
      </div>
	</div>
</div>
<div class="container">
      <div class="row">
      		<div class="col-md-6 ">
				<img src="http://tmmemorabilia.kylakiefer.com/wp-content/uploads/2016/11/about.jpg" class="img-responsive img-circle about-img" alt="About Image">
	      	</div>
	     	<div class="col-md-6">
				<h2>About TM Memorabilia</h2>
				<p><b>TM Memorabilia LLC Sports Collectibles has been in business full-time since 2004.</b></p>
				<p>As a 45 year sports collector myself, I run my business with the collector in mind.</p>
				<p>My philosophy is that my customers have the peace of mind to purchase with confidence as I offer good value and 100% authentic signed items.</p>
				<p>Our passion is offering many vintage all sport items as well as current team star items.</p>
				<p><b>We sell only 100% authenticated sports memorabilia.</b></p>
	      </div>
      </div>
</div>

</div>
<?php get_footer(); ?>
