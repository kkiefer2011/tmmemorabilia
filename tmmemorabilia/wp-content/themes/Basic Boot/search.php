<?php get_header(); ?>
<div class="container">
      <div class="row">
	      <div class="col-md-12">
				<div class="container">
				
				      <div class="row">
					      
					  <div class="col-md-3">
					  	<?php if ( is_active_sidebar( 'sidebar_right' ) ) : ?>
							<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
								<?php dynamic_sidebar( 'sidebar_right' ); ?>
							</div><!-- #primary-sidebar -->
						<?php endif; ?>		  
					  </div>
					      
					      
					      
					      <div class="col-md-9">
				
							<h2>Search Results for '<?php echo get_search_query(); ?>'</h2>	
							
							<?php if ( function_exists( 'ngg_images_results' ) ) ngg_images_results(); ?>
							
							
							<?php if ( have_posts() ): ?>
							<ol>
							<?php while ( have_posts() ) : the_post(); ?>
								<li>
									<article>
										<h2><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
										<time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_date(); ?> <?php the_time(); ?></time> <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?>
										<?php the_content(); ?>
									</article>
								</li>
							<?php endwhile; ?>
							</ol>
							<?php else: ?>
							
							<?php endif; ?>
							</div>
					
					  
					  
				      </div>
				</div>
	      </div>
      </div>
</div>
<?php get_footer(); ?>