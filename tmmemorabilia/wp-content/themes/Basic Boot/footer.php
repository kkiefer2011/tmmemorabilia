	<div class="footer">
		<div class="container">
	      <div class="row">
	      	 <div class="col-md-5 footer-left">
             <ul style="color: #fff;">
  				<li style="display: inline"><a class="footer-menu" href="http://tmmemorabilia.kylakiefer.com/">Home</a></li> |
  				<li style="display: inline"><a class="footer-menu" href="http://tmmemorabilia.kylakiefer.com/index.php/shop/">Shop</a></li> |
  				<li style="display: inline"><a class="footer-menu" href="http://tmmemorabilia.kylakiefer.com/index.php/featured/">Fundraising</a></li> |
  				<li style="display: inline"><a class="footer-menu" href="http://tmmemorabilia.kylakiefer.com/index.php/featured/">Featured</a></li>
			</ul>
	      	 </div>
             <div class="col-md-3 footer-text">
              <p>TM Memorabilia © 2016</p>
	      	 </div>
		  	 <div class="col-md-4 footer-right">
		      <ul style="color: #fff;">
  				<li style="display: inline"><a href="default.asp"><img class="footer-pad" src="http://tmmemorabilia.kylakiefer.com/wp-content/uploads/2016/11/fa-phone_75_0_509623_none.png" alt="Smiley face" height="25" width="25"></a></li>
  				<li style="display: inline"><a  href="mailto:tm40@hotmail.com"><img class="footer-pad" src="http://tmmemorabilia.kylakiefer.com/wp-content/uploads/2016/11/fa-envelope_75_0_509623_none.png" alt="Smiley face" height="25" width="25"></a></li>
  				<li style="display: inline"><a  href="https://www.facebook.com/TmMemorabilia/?fref=ts"><img class="footer-pad" src="http://tmmemorabilia.kylakiefer.com/wp-content/uploads/2016/11/brandico-facebook-rect_75_0_509623_none.png" alt="Smiley face" height="25" width="25"></a></li>
			</ul>
	      	 </div>
	      </div>
		</div>
    </div>
<?php wp_footer(); ?>
</body>
</html>